<?php

// src/AppBundle/Controller/ProductController.php
namespace AppBundle\Controller;

use AppBundle\Service\ProductRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductController extends Controller
{
  public function productListAction()
  {

    $repo = $this->container->get('app.product_repository');
    $repo2 = $this->container->get('app.product_repository');
    $products = $repo->findAll();

    return $this->render(
      'product/product.html.twig',
      array('products' => $products)
    );
  }
}

?>
