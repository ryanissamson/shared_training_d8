<?php

namespace AppBundle\Service;

// src/AppBundle/Service/ProductRepository.php
use AppBundle\Model\Product;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ProductRepository
{
  private $logger;
  private $pdo;

  public function __construct(LoggerInterface $logger, \PDO $pdo)
  {
    $this->logger = $logger;
    $this->pdo = $pdo;
  }

  public function findAll() {
    $logger = $this->logger;
    $logger->alert('FOOOO');
    $productsData = $this->pdo->query('SELECT * FROM product')->fetchAll();
    $products = [];
    foreach ($productsData as $productData) {
        $product = new Product();
        $product->setId($productData['id']);
        $product->setName($productData['name']);
        $product->setDescription($productData['description']);
        $product->setPrice($productData['price']);
        $products[] = $product;
    }

  return $products;
  }
}
